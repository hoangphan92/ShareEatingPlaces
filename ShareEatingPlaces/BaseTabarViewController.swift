//
//  BaseTabarViewController.swift
//  ShareEatingPlaces
//
//  Created by iOS Student on 4/11/17.
//  Copyright © 2017 HoangPhan. All rights reserved.
//

import UIKit

class BaseTabarViewController: UITabBarController,UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        
        let homeVC = storyboard.instantiateViewController(withIdentifier: "homeVC") as! HomeViewController
        let searchVC = storyboard.instantiateViewController(withIdentifier: "searchVC") as! SearchLocationViewController
        let userVC = storyboard.instantiateViewController(withIdentifier: "userVC") as! UserCotroller
        let locationVC = storyboard.instantiateViewController(withIdentifier: "locationVC") as! FindLocationViewController
        
        
        let homeNav = BaseNavigationController(rootViewController: homeVC)
        let locationNav = BaseNavigationController(rootViewController: locationVC)
        let searchNav = BaseNavigationController(rootViewController: searchVC)
        let userNav = BaseNavigationController(rootViewController: userVC)
        viewControllers = [homeNav,locationNav,searchNav,userNav]
        
        
        settingNavForEachController(viewcontroller: homeVC, tranparent: false, navTitle: "Home", tabbarTitle: "", image: "home-gray", selectedImage: "home-red")
        settingNavForEachController(viewcontroller: searchVC, tranparent: false, navTitle: "Search", tabbarTitle: "", image: "search-gray", selectedImage: "search-red")
         settingNavForEachController(viewcontroller: locationVC, tranparent: false, navTitle: "Location", tabbarTitle: "", image: "location-gray", selectedImage: "location-red")
        settingNavForEachController(viewcontroller: userVC, tranparent: false, navTitle: "UserProfile", tabbarTitle: "", image: "user-gray", selectedImage: "user-red")
    }
    
    func settingNavForEachController(viewcontroller: BaseViewController , tranparent :Bool , navTitle :String , tabbarTitle :String, image: String, selectedImage: String)  {
        viewcontroller.navigationItem.title = navTitle
        viewcontroller.tabBarItem = UITabBarItem(title: tabbarTitle, image: UIImage(named: image)?.withRenderingMode(.alwaysTemplate), selectedImage: UIImage(named: selectedImage)?.withRenderingMode(.alwaysOriginal))
        viewcontroller.tranparent = tranparent
    }

}

//
//  AddLocationViewController.swift
//  ShareEatingPlaces
//
//  Created by Hoang on 4/16/17.
//  Copyright © 2017 HoangPhan. All rights reserved.
//

import UIKit

class AddLocationViewController: BaseViewController, DistrictOption{
    
    @IBOutlet weak var tf_nameLocation: CustomTextField!
    
    @IBOutlet weak var tf_locationAddress: CustomTextField!
    
    @IBOutlet weak var tf_phoneAddress: CustomTextField!
    
    @IBOutlet weak var bt_district: CustomButton!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(white: 1, alpha: 0.95)
        setColorPlacehoder()
    }
    
    
    func setColorPlacehoder()  {
        tf_nameLocation.attributedPlaceholder = NSAttributedString(string: tf_nameLocation.placeholder!, attributes: [NSForegroundColorAttributeName : UIColor.gray.withAlphaComponent(0.5)])
        tf_locationAddress.attributedPlaceholder = NSAttributedString(string: tf_locationAddress.placeholder!, attributes: [NSForegroundColorAttributeName : UIColor.gray.withAlphaComponent(0.5)])
        tf_phoneAddress.attributedPlaceholder = NSAttributedString(string: tf_phoneAddress.placeholder!, attributes: [NSForegroundColorAttributeName : UIColor.gray.withAlphaComponent(0.5)])
    }
    func setDistrict(districtName: String) {
       bt_district.setTitle(districtName, for: .normal)
    }
    @IBAction func action_chooseDistrict(_ sender: UIButton) {
        let districtView = self.storyboard?.instantiateViewController(withIdentifier: "districtVC") as! DistrictPopUpVCViewController
        self.navigationController?.present(districtView, animated: true, completion: nil)
        districtView.delegate = self
    }
    
    @IBAction func action_time_on(_ sender: UIButton) {
    }
    @IBAction func action_time_out(_ sender: UIButton) {
    }
    
}


//
//  DistrictPopUpVCViewController.swift
//  ShareEatingPlaces
//
//  Created by iOS Student on 4/17/17.
//  Copyright © 2017 HoangPhan. All rights reserved.
//

import UIKit
protocol DistrictOption {
    func setDistrict(districtName: String)
}
class DistrictPopUpVCViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var listDistrict = ["Quận Nam Từ Liêm" , "Quận Bắc Từ Liêm", "Quận Ba Đình"]
    @IBOutlet weak var viewTotal: UIView!
    var delegate: DistrictOption! = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.2)
        viewTotal.layer.cornerRadius = 10
        viewTotal.layer.masksToBounds = true
    
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listDistrict.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.font = UIFont.systemFont(ofSize: 12)
        cell.textLabel?.text = listDistrict[indexPath.row]
        tableView.separatorStyle = .singleLine
       
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let name = listDistrict[indexPath.row]
        delegate.setDistrict(districtName: name)
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func action_ignore(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
}
